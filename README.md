Provide a ``make srpm`` copr target git repository. Builds the latest Fedora
28 kernel with a patch for the threadripper workaround.


Developed for use with the [threadripper passthrough copr](https://copr.fedorainfracloud.org/coprs/jefferym/kernel-threadripper-passthrough/).
Incorporates the patch needed for threadripper passthrough to work
as mentioned in the [reddit thread](https://www.reddit.com/r/Amd/comments/7gp1z7/threadripper_kvm_gpu_passthru_testers_needed/).

To use this add this repository as the ``make srpm`` git url target of a copr build.

This was tested with the motherboard ``Gigabyte X399 AORUS Gaming 7 r1``.
With firmware revision ``F3j`` the patch was required to be applied.

Upgrading the moetherboard's firmware to ``F10`` fixes this bug and the
workaround is no longer needed.

